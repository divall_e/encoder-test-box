
#include <SoftwareSerial.h>
//SoftwareSerial mySerial(3,2);//RX/TX
#include <TFT.h>  // Arduino LCD library
#include <SPI.h>

// pin definition for the Uno
#define cs   10
#define dc   9
#define rst  8
const int buttonPin1 = 4;
const int buttonPin2 = 5;
#define encoder0PinA  2
#define encoder0PinB  3
//volatile unsigned int encoder0Pos = 0;
long encoder0Pos = 0;
long oldencoder0Pos = -1;
TFT TFTscreen = TFT(cs, dc, rst);
long pos;
//int posy;
//char c;
char printout[10];
char bitsout[2];
char cr=13;
long reading=0;  //the position
long oldreading=1;
long lhsreading=0;
long raw;
long offset=0;   //when zero gives raw value
int  type;     // BISS=2 SSI=1
int  error;    //error
float fware;   //firmware version
int  bits;     //num bits
String ss;
int old;       //old data
int cnt=500;       //counter

int setmode=1; // 1=set SSI 2=set BISS 
int inconoff = 0;
int setbits=24;//8-30
int kursor=0; //0=ssi/biss 1=bits 2=zero 3=raw
int bs1;        //button states to look for edges
int bs2;
int oldbs1;
int oldbs2;
void setup() {
  // put your setup code here, to run once:
  pinMode(encoder0PinA, INPUT); 
  digitalWrite(encoder0PinA, HIGH);       // turn on pullup resistor
  pinMode(encoder0PinB, INPUT); 
  digitalWrite(encoder0PinB, HIGH);       // turn on pullup resistor

  //attachInterrupt(0, doEncoder, CHANGE);  // encoder pin on interrupt 0 - pin 2
  
  
  Serial.begin(115200);
  //mySerial.begin(115200);
   // set the font color to white
  TFTscreen.stroke(255, 255, 255);
  // set the font size
  TFTscreen.setTextSize(3);

  TFTscreen.begin();
  TFTscreen.background(0,0,0);
  
  pinMode(buttonPin1, INPUT);
  pinMode(buttonPin2, INPUT);
  old=-1;//force display update
  //Serial.println("Init complete");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (setmode != 0) {  //if not incremental only then read protura module
    Serial.write ("#");
    delay(1);
    Serial.write ("C");
    delay(1);
  }
  if(Serial.available() > 0){
    TFTscreen.stroke(0,0,0);        //delete
    ss=String(reading);
    raw=Serial.parseInt();
    //raw=encoder0Pos;
    reading=raw-offset;
    ss.toCharArray(printout,10);
    TFTscreen.setTextSize(3);
    if (reading!=oldreading){      //if reading hasn't changed then don't delete
      TFTscreen.text(printout, 0,0);
      oldreading=reading;
    }
    TFTscreen.stroke(255,255,255);  //and rewrite
    ss=String(reading);
    ss.toCharArray(printout,10);
    TFTscreen.text(printout, 0,0);
    
    error=Serial.parseInt();
    type=Serial.parseInt();
    fware=Serial.parseFloat();
    bits=Serial.parseInt();
    setbits=bits;
    //c = Serial.read();
    //Serial.write(c);

    //cstr[0]=65;

    TFTscreen.stroke(0,0,0);
    TFTscreen.rect(cnt, 23,3,3);
    cnt=cnt+1;
    if (cnt>155) {cnt=1;lhsreading=reading;}
    TFTscreen.stroke(0,255,0);
    TFTscreen.rect(cnt, 23,3,3);  
    TFTscreen.stroke(0,0,0);
    //TFTscreen.line(cnt, 50,cnt,127);

    pos=91+(lhsreading-reading)/10000;  
    if ((pos>50)&&(pos<128)){
      TFTscreen.stroke(0,255,0);
      //TFTscreen.point(cnt, pos);
    }
    pos=92+(lhsreading-reading)/100000;  
   if ((pos>50)&&(pos<128)){
      TFTscreen.stroke(0,0,255);
      //TFTscreen.point(cnt, pos);
    }
    pos=90+lhsreading-reading;
    if ((pos>50)&&(pos<128)){
      TFTscreen.stroke(255,0,0);
      //TFTscreen.point(cnt, pos);
    }
  }//end if serial available
  
  
  //update the display
    if (old!=(bits*20+type+inconoff*8)){
      TFTscreen.background(0,0,0);
      TFTscreen.setTextSize(2);
      TFTscreen.stroke(255,255,255);
      if (type==1){TFTscreen.text("SSI    BIT ZR", 0,30) ;}
      if (type==2){TFTscreen.text("BISS   BIT ZR", 0,30);}
      ss=String(bits);
      ss.toCharArray(printout,10);
      TFTscreen.text(printout, 60,30);
      TFTscreen.stroke(255,255,0);
      if (kursor==0) {  TFTscreen.rect(  0,48,47,1);} //underline BISS/SSI
      if (kursor==1) {  TFTscreen.rect( 60,48,59,1);} //underline bits
      if (kursor==2) {  TFTscreen.rect(132,48,11,1);} //underline Z
      if (kursor==3) {  TFTscreen.rect(144,48,11,1);} //underline R 
      if (kursor==4) {  TFTscreen.rect( 50,68,30,1);} //underline inc on/off  
      if (kursor==5) {  TFTscreen.rect( 96,68,11,1);} //underline INC Z  
      TFTscreen.stroke(10,255,255);      
      if (inconoff==0){TFTscreen.text("INC:OFF Z", 0,52) ;}
      if (inconoff==1){TFTscreen.text("INC:ON  Z", 0,52) ;}
      oldencoder0Pos=encoder0Pos+9999;
      old=bits*20+type+inconoff*8;
    }
    
  
  
  
  
  
   bs1=digitalRead(buttonPin1);
    if ((bs1 == HIGH) && (bs1!=oldbs1) ) {//first button cycles round options with kursor
     TFTscreen.stroke(0,0,0);
     TFTscreen.rect(0,48,159,1);  //delete old cursors
     TFTscreen.rect(0,68,159,1);  //delete old cursors
     kursor=kursor+1;
     if (kursor>5) {kursor=0;}
     TFTscreen.stroke(255,255,0);
     //if (kursor==0) {  TFTscreen.rect(  0,48,47,1);} //underline BISS/SSI /18 pixels lower
     //if (kursor==1) {  TFTscreen.rect( 60,48,59,1);} //underline bits
     //if (kursor==2) {  TFTscreen.rect(132,48,11,1);} //underline Z
     //if (kursor==3) {  TFTscreen.rect(144,48,11,1);} //underline R 
     //if (kursor==4) {  TFTscreen.rect(  0,80,30,1);} //underline inc on/off  
     //if (kursor==5) {  TFTscreen.rect(101,70,30,1);} //underline INC Z  
     old=-1; 
    }  //end if button 1 pressed
    bs2=digitalRead(buttonPin2);
    if ((bs2 == HIGH) && (bs2!=oldbs2) && (kursor==0) ) {//change between SSI and BISS
      if (setmode==1){
        setmode=2;
        Serial.write ("c2"); 
        Serial.write (cr);
        TFTscreen.text("B", 10,60);
      }  
      else {
        setmode=1;
        Serial.write ("c1"); 
        Serial.write (cr);
        TFTscreen.text("S", 10,60);
      }          
    }  //end if button 2 and kursor=0 pressed


    if ( (bs2== HIGH) && (bs2!=oldbs2) && (kursor==1) ) { //inc number of bits
      setbits=setbits+1;
      if (setbits>30){setbits=10;}
      ss=String(setbits);
      ss.toCharArray(bitsout,3);
      Serial.write ("c");
      Serial.write (bitsout);
      Serial.write (cr);
      TFTscreen.text(bitsout, 10,60);
    }//end inc bits pressed 
    if ( (bs2== HIGH) && (bs2!=oldbs2) && (kursor==2) ) { //Zero
      offset=raw;
    }//end zero
    if ( (bs2== HIGH) && (bs2!=oldbs2) && (kursor==3) ) { //zero the zero! ie. show raw data
      offset=0;
    }//end raw    

    
     if ( (bs2== HIGH) && (bs2!=oldbs2) && (kursor==4) ) { //INC on off
      old=-1;
      if (inconoff==0){inconoff=1;attachInterrupt(0, doEncoder, CHANGE);}
      else            {inconoff=0;detachInterrupt(0);}
    }//
    
    if ( (bs2== HIGH) && (bs2!=oldbs2) && (kursor==5) ) { //Zero incremental
      encoder0Pos=0;
    }//zero incremental
    
    
    
    
    
  if (encoder0Pos!=oldencoder0Pos){     
    TFTscreen.setTextSize(3);   
    TFTscreen.stroke(0,0,0);        //black
    ss=String(oldencoder0Pos);
    ss.toCharArray(printout,10);
    TFTscreen.text(printout, 0,100);
    oldencoder0Pos=encoder0Pos;
    
    TFTscreen.stroke(10,255,255);  //and rewrite
    ss=String(oldencoder0Pos);
    ss.toCharArray(printout,10);
    TFTscreen.text(printout, 0,100);
  }
    
        oldbs2=bs2;
    oldbs1=bs1;
    //if (bs1==0){TFTscreen.stroke(0,0,0);}
    //if (bs1==1){TFTscreen.stroke(255,255,255);}
    //TFTscreen.text("1", 10,80);
    //if (bs2==0){TFTscreen.stroke(0,0,0);}
    //if (bs2==1){TFTscreen.stroke(255,255,255);}
    //TFTscreen.text("2", 30,80);
  //Serial.println(">");
  delay(120);

}



void doEncoder() {
  /* If pinA and pinB are both high or both low, it is spinning
   * forward. If they're different, it's going backward.
   *
   * For more information on speeding up this process, see
   * [Reference/PortManipulation], specifically the PIND register.
   */
  if (digitalRead(encoder0PinA) == digitalRead(encoder0PinB)) {
    encoder0Pos++;
  } else {
    encoder0Pos--;
  }

  
}
