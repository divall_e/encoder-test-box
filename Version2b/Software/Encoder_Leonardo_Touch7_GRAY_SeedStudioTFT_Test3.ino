/***************************************************
  This is our touchscreen painting example for the Adafruit ILI9341 Shield
  ----> http://www.adafruit.com/products/1651

  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

//Display from Waveshare working
//Changed to SPI_MOde0 to get the ICHaus module working
//Enables for Line-Receiver added (not tested)

//http://rcarduino.blogspot.com/2013/04/the-problem-and-solutions-with-arduino.html  

//need to install ST7789 library

// Changes from Adafruit TFT for SeedStudio..  .. look for //SS Change//
// ST77XX_Colours --> ST77XX_
//turn on screen is also needed on D9

//D3  TP_IRQ  Touch panel interrupt
//D4  TP_CS Touch panel chip select
//D5  SD_CS Micro SD card chip select
//D7  LCD_DC  LCD data/command selection
//D9  LCD_BL  LCD backlight control
//D10 LCD_CS  LCD chip select
//D11 MOSI  SPI data input
//D12 MISO  SPI data output
//D13 SCLK  SPI clock


#include <Adafruit_GFX.h>    // Core graphics library
#include <SPI.h>
#include <Wire.h>      // this is needed even tho we aren't using it
//SS Change// #include <Adafruit_ILI9341.h>
//SS Change// #include <Adafruit_STMPE610.h>
#include <Adafruit_ST7789.h> // Hardware-specific library for ST7789
#include "XPT2046.h"         // touch stuff
uint16_t TSXpos;
uint16_t TSYpos;

// This is calibration data for the raw touch data to the screen coordinates
#define TS_MINX 120 //100   
#define TS_MINY 120 //70
#define TS_MAXX 990
#define TS_MAXY 930


// The STMPE610 uses hardware SPI on the shield, and #8
//SS Change - touch screen is differnt//#define STMPE_CS 4
//SS Change                           //Adafruit_STMPE610 ts = Adafruit_STMPE610(STMPE_CS);

// The display also uses hardware SPI, plus #9 & #10
#define TFT_CS        10
#define TFT_RST        -1 // Or set to -1 and connect to Arduino RESET pin
#define TFT_DC         7
//SS Change//Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);
Adafruit_ST7789 tft = Adafruit_ST7789(TFT_CS, TFT_DC, TFT_RST);
//Dark grey not in ST7789 library
byte ST77XX_DARKGREY=55;

// Size of the color selection boxes and the paintbrush size
#define BOXSIZE 40
#define PENRADIUS 3
int oldcolor, currentcolor;
int posx,posy;


//Encoder variables
byte absMode = 1;  //0=off 1=SSI 2=BISS
byte absBits = 26;
byte incMode = 0;  //0=off 1=on
byte rawMode = 0;  //0=raw data 1=zeroed
byte Ebits   = 2;  //error bits to show
byte ShowDot = 0;  //show the dot 1 or 0


#define encoder0PinA  2
#define encoder0PinB  3
#define index0PinC  0
#define print0PinD  1

#define ICHausPin  A2
#define ICResetPin A3
#define ICLS32EN   A4
#define ICLS32NEN  A5

long encoder0Pos = 0;
long oldencoder0Pos = -1;

long oldAbsPos,AbsPos,AbsZero;
byte oldEbits;

char printout[10];
char printoutA[10];
char lastInc [10];
char lastAbs [10];
String ss;

byte PrintFlag;
byte IndexState,oldIndexState;
long IndexCount,oldIndexCount;
char lastIndex [10];


//0-3 are used locally 4-7 store data from slave system
//byte MB4SelPin [] = {10,11,12,13};qq
byte MB4Bits[8]  = {24, 24, 24, 24, 24, 24, 24, 24};//Encoder bits for the 8 channels
byte MB4EBits[8] = {0,  0,  0,  0,  0,  0,  0,  0};        //Extra bits for the 8 channels
byte MB4Type[8]  = {1,  2,  2,  2,  2,  2,  2,  2};        //Encoder type: 0=BissB; 1=BissC; 2=SSI  3=SSI Gray
byte MB4Freq[8]  = {0x13,0x13,0x13,0x13,0x13,0x13,0x13,0x13};//Clock Freq- 0x13=250kHz 
byte MB4Stat[8]  = {0, 0,0, 0, 0, 0, 0, 0};        //Status: 0=OFF 1=ON/try and use 
byte MB4Version[8] = {0, 0, 0, 0, 0, 0, 0, 0} ; 
byte MB4Comm[8]  = {0, 0, 0, 0, 0, 0, 0, 0};        //Comms 1=OK(Version<>0) 0=not tested 2=error
byte MB4StatF0[8]  = {0, 0, 0, 0, 0, 0, 0, 0};        //Status byte after read
byte MB4Raw[64] ;                                   //8 bytes of raw data read back for each channel
byte LMMFound[8]  = {0, 0, 0, 0, 0, 0, 0, 0};        //1=LMM;0=MFORCE 
char Letters[8] ="ABCDEFGH";
char Numbers[10] ="0123456789";
unsigned long MB4Num[8] = {0, 0, 0, 0, 0, 0, 0, 0};    //Raw as a 32 bit number 
byte MB4Errors[8];//data from the error bits: bit0=warning;bit1=error (0=error in each case)
byte MB4OldErrors[8]={0, 0, 0, 0, 0, 0, 0, 0};

//Read/write on SPI (on MB4)
const byte WcommandPacket = 0x02;
const byte RcommandPacket = 0x03;
unsigned long unsl=0;
byte su;




void setup(void) {
 // while (!Serial);     // used for leonardo debugging
 
  Serial.begin(115200);
 // while (!Serial){};   //wait for serial...neededfor leonardo
  
  Serial.println(F("Start up"));

  SPI.setDataMode(SPI_MODE0);  //0 "works" for all
  SPI.setClockDivider(SPI_CLOCK_DIV16);  // 1 MHz  ... DIV4
  SPI.setBitOrder(MSBFIRST);

  
  //SS Change//tft.begin();
  tft.init(240, 320,SPI_MODE0); 
  pinMode(9, OUTPUT);     //Turn on the LCD light
  digitalWrite(9, HIGH); 
/* //SS Change//  remove old touch screen stuff   
  if (!ts.begin()) {
    Serial.println("Couldn't start touchscreen controller");
    while (1);
  }
  Serial.println("Touchscreen started");
*/  
  SPI.setDataMode(SPI_MODE3);
  Xpt.xpt2046_init();  //replacement touch screen init.
  SPI.setDataMode(SPI_MODE0);
  
  tft.setRotation(1);
  // make top row boxes

  // select the current color 'red'
  //tft.drawRect(0, 0, BOXSIZE, BOXSIZE, ST77XX_WHITE);
  currentcolor = ST77XX_RED;

//inc encoder
  pinMode(encoder0PinA, INPUT); 
  digitalWrite(encoder0PinA, HIGH);       // turn on pullup resistor
  pinMode(encoder0PinB, INPUT); 
  digitalWrite(encoder0PinB, HIGH);       // turn on pullup resistor


  pinMode(ICLS32EN, OUTPUT);
  digitalWrite(ICLS32EN, HIGH);           //Enable for Inc Line Receiver
  pinMode(ICLS32NEN, OUTPUT);
  digitalWrite(ICLS32NEN, LOW);           //Not-Enable for Inc Line Receiver


//index
  pinMode(index0PinC, INPUT); 
  digitalWrite(index0PinC, HIGH);       // turn on pullup resistor
//OPtion//print  
  pinMode(print0PinD, INPUT); 
  digitalWrite(print0PinD, HIGH);       // turn on pullup resistor

  attachInterrupt(0, doEncoder0, CHANGE);
  attachInterrupt(1, doEncoder1, CHANGE);
  attachInterrupt(2, doIndex2, CHANGE);
  attachInterrupt(3, doPrint3, RISING);
//abs encoder
  pinMode     (ICHausPin, OUTPUT); 
  digitalWrite(ICHausPin, HIGH); 

  
  pinMode     (ICResetPin, OUTPUT); 
  digitalWrite(ICResetPin, LOW); 
  delay(100);
  digitalWrite(ICResetPin, HIGH);
  delay(100);

  

  oldEbits=255;PrintFlag=9;IndexCount=0;
  IndexState=
  MB4CheckComms(0);

  SetupMB4Channel(0);

    RedrawDisplay(0);
}




void RedrawDisplay(byte box){//0=all  1-4 is top row  5-8 is second   9-12 is bottom
  byte i;
  if (box==0){tft.fillScreen(ST77XX_BLACK);}

  // make top box boxes  
  if (box<4){
      tft.setTextColor(ST77XX_YELLOW);    tft.setTextSize(3);
      if (box==1){tft.fillRect(0,   0, 78,  49, ST77XX_BLACK);}
      tft.drawRect(0,   0, 78,  49, ST77XX_BLUE);tft.setCursor(5,   9);
      if (absMode==0){tft.setTextColor(ST77XX_RED);tft.setTextSize(2);
        tft.setCursor(16,  6);tft.println("ABS");
        tft.setCursor(16,  26);tft.println("OFF");
      }  
      if (absMode==1){tft.setTextColor(ST77XX_YELLOW);tft.println("SSI");}
      if (absMode==2){tft.setTextColor(ST77XX_YELLOW);tft.println("BISS");}
      if (absMode==3){tft.setTextColor(ST77XX_YELLOW);tft.println("GRAY");}
      tft.setTextColor(ST77XX_YELLOW);    tft.setTextSize(3);

      if (box==2){tft.fillRect(80,  0, 78, 49, ST77XX_BLACK);}
      tft.drawRect(80,  0, 78, 49, ST77XX_BLUE);tft.setCursor(86,  9);tft.println(absBits,DEC);
      if (((absMode!=2) ) || (Ebits!=2)){tft.setTextColor(ST77XX_BLACK);}
      tft.setTextSize(2);tft.setCursor(126,  29);tft.println("+2");          
      tft.setTextSize(3);
      tft.setTextColor(ST77XX_YELLOW);
      


      if (box==3){tft.fillRect(160, 0, 78, 49, ST77XX_BLACK);}
      tft.drawRect(160, 0, 78, 49, ST77XX_BLUE);tft.setTextSize(2);
      if (incMode==0){
        tft.setTextColor(ST77XX_RED);
        tft.setCursor(176,  6);tft.println("INC"); tft.setCursor(176,  26);tft.println("OFF");  
      }
     if (incMode==1){
        tft.setTextColor(ST77XX_CYAN);
        tft.setCursor(176,  6);tft.println("INC"); tft.setCursor(176,  26);tft.println("ON");  
      }
       tft.setTextColor(ST77XX_YELLOW);    tft.setTextSize(3);  
   }

   if ((box==0) ){
      tft.setTextSize(2);
      tft.drawRect(240,  0, 78, 49, ST77XX_BLUE);tft.setCursor(252,  9);tft.println("CLR");
    
      tft.setTextColor(ST77XX_CYAN  ) ;tft.setCursor(2,  68);tft.println("INC:");
      tft.setTextColor(ST77XX_WHITE) ;tft.setCursor(2, 106);tft.println("ABS:");
   }
    tft.setTextColor(ST77XX_YELLOW) ;tft.setTextSize(3);
//< presets >
  if ((box==0) || (box==3)){
      tft.drawRect(0,   140, 78, 49, ST77XX_BLUE);tft.setCursor(6,   149);tft.println(" <");
      tft.drawRect(80,  140, 78, 49, ST77XX_BLUE);tft.setCursor(86,  149);tft.println(" 26");
      tft.drawRect(160, 140, 78, 49, ST77XX_BLUE);tft.setCursor(166, 149);tft.println(" 32");
      tft.drawRect(240, 140, 78, 49, ST77XX_BLUE);tft.setCursor(246, 149);tft.println(" >");
  }    
 //bottom line
   if ((box==0) || (box>8)){
      tft.setTextSize(2);
      tft.setTextColor(ST77XX_CYAN);
      tft.drawRect(0,   190, 78, 49, ST77XX_BLUE);tft.setCursor(16,  199);tft.println("ZERO") ;tft.setCursor(16,  219);tft.println("INC");
      tft.setTextColor(ST77XX_YELLOW);
      tft.drawRect(80,  190, 78, 49, ST77XX_BLUE);tft.setCursor(96,  199);tft.println("ZERO") ;tft.setCursor(96,  219);tft.println("ABS");
      if (box==11){tft.drawRect(160, 190, 78, 49, ST77XX_BLACK);}
      tft.drawRect(160, 190, 78, 49, ST77XX_BLUE);
      if (rawMode==0){tft.setTextColor(ST77XX_YELLOW);} else  {tft.setTextColor(ST77XX_DARKGREY);}
      tft.setCursor(172, 199);tft.println("RAW /");
      if (rawMode==1){tft.setTextColor(ST77XX_YELLOW);} else  {tft.setTextColor(ST77XX_DARKGREY);}
      tft.setCursor(166, 219);tft.println("ZEROED");tft.setTextColor(ST77XX_YELLOW);
      if (Ebits==2){tft.setTextColor(ST77XX_YELLOW);} else  {tft.setTextColor(ST77XX_DARKGREY);}
      tft.drawRect(240, 190, 78, 49, ST77XX_BLUE);tft.setCursor(256, 199);tft.println("EBIT") ;tft.setCursor(246, 219);tft.println("TOGGLE"); 
      tft.setTextColor(ST77XX_YELLOW);
   }

  if (MB4Comm[0]!=1){
    tft.setTextSize(2);
    tft.setTextColor(ST77XX_YELLOW) ;tft.setCursor(65,  103);tft.println("MODULE COMMS ERROR");
  }
  
  if ((box==0) ){  for(i=0; i<10; i++) {lastInc[i]=65;lastAbs[i]=65;}   }

  
}



void Press(int XX, int YY){
byte rd;
  rd=255;
  if (YY<50){//top row
   if (XX<80){//first button
    absMode++;if (absMode>3){absMode=0;}rd=1;su=1;
   }
   else if (XX<160){//2nd button!

   }
   else if (XX<240){//3rd button
    incMode=((incMode+1)&1);rd=3;
    tft.fillRect(315,64,5,5, ST77XX_BLACK);
    tft.fillRect(315,74,5,5, ST77XX_BLACK);
    tft.fillRect(315,84,5,5, ST77XX_BLACK);
   }    
   else if (XX<320){//3rd button
    ShowDot=((ShowDot+1)&1);rd=0;MB4CheckComms(0);
    
   }   
  }//end top row

  if ((YY>140) && (YY<190)){//3rd tow
   if (XX<80){//<
    absBits--;if (absBits<10){absBits=10;}rd=2;su=1;
   }
   else if (XX<160){//2nd button!
    absBits=26;rd=2;su=1;
   }
   else if (XX<240){//3rd button
    absBits=32;rd=2;su=1;
   }  
      else if (XX<320){//> button
      absBits++;if (absBits>36){absBits=36;}rd=2;su=1;
   }    
  }//end 3rd row



  if ((YY>190) && (YY<240)){//bottom tow
   if (XX<80){//zero inc
    encoder0Pos=0;
   }
   else if (XX<160){//2nd button!
    AbsZero=MB4Num[0];
   }
   else if (XX<240){//3rd button
    rawMode=((rawMode+1)&1);rd=11;
   }  
   else if (XX<320){//> button
      Ebits=((Ebits+2)&2);rd=2;su=1;
   }    
  }//end bottom row




  if (rd != 255){RedrawDisplay(rd);delay(100);}


/* //SS Change//  remove old touch screen stuff 
  while (! ts.bufferEmpty()){
     TS_Point p = ts.getPoint();
  }
*/

}



void DrawInc(){
byte i;
//encoder count
    if (encoder0Pos!=oldencoder0Pos){     
      tft.setTextSize(4);
      ss=String(encoder0Pos);
      ss.toCharArray(printout,10);

      for(i=0; i<8; i++) {
        if (printout[i]!=lastInc[i]){
          tft.setTextColor(ST77XX_BLACK );tft.setCursor(60+i*25,  62);tft.println(lastInc [i]);
          tft.setTextColor(ST77XX_CYAN) ;tft.setCursor(60+i*25,  62);tft.println(printout[i]);
          lastInc[i]=printout[i];       
        }
        printout[i]=32;//set to a space so there is something to check against if the next num has less than 10 chars
      }         
    oldencoder0Pos=encoder0Pos;  
    }
//index mark  (IndexState!=oldIndexState) ||
    if ( (IndexCount!=oldIndexCount) ){     
      if (IndexCount>999){IndexCount=0;}
      tft.setTextSize(4);
      ss=String(IndexCount);
      ss.toCharArray(printout,10);

      for(i=0; i<3; i++) {
        if (printout[i]!=lastIndex[i]){
          tft.setTextColor(ST77XX_BLACK );tft.setCursor(235+i*25,  62);tft.println(lastIndex [i]);
          tft.setTextColor(0x5AEB)        ;tft.setCursor(235+i*25,  62);tft.println(printout[i]);
          lastIndex[i]=printout[i];       
        }
        printout[i]=32;//set to a space so there is something to check against if the next num has less than 10 chars
      }         
    oldIndexCount=IndexCount;  
    }

   // if (IndexState==0){tft.fillRect(315,70,5,5, ST77XX_MAGENTA);}
   // else {tft.fillRect(315,70,5,5, ST77XX_WHITE);}

    if (digitalRead(3)==0){tft.fillRect(315,64,5,5, 0x5AEB);}
    else {tft.fillRect(315,64,5,5, ST77XX_GREEN);}
    if (digitalRead(2)==0){tft.fillRect(315,74,5,5, 0x5AEB);}
    else {tft.fillRect(315,74,5,5, ST77XX_GREEN);}
    if (digitalRead(0)==0){tft.fillRect(315,84,5,5, 0x5AEB);}
    else {tft.fillRect(315,84,5,5, ST77XX_WHITE);}
 
  tft.setTextSize(3);

  
}

void DrawAbs(){
byte i,newEbits;

    if ((AbsPos!=oldAbsPos) && (MB4Comm[0]==1)){     
      tft.setTextSize(4);
      ss=String(AbsPos);
      ss.toCharArray(printoutA,10);

      for(i=0; i<10; i++) {
        if (printoutA[i]!=lastAbs[i]){
          tft.setTextColor(ST77XX_BLACK );tft.setCursor(60+i*25,  98);tft.println(lastAbs [i]);
          tft.setTextColor(ST77XX_WHITE) ;tft.setCursor(60+i*25,  98);tft.println(printoutA[i]);
          lastAbs[i]=printoutA[i];       
        }
        printoutA[i]=32;//set to a space so there is something to check against if the next num has less than 10 chars
      }         
   oldAbsPos=AbsPos;  
  }

  newEbits=4;//turn off
  if (Ebits==2){newEbits=MB4Errors[0];}//if bits on then copy values
  if ((absMode!=2)){newEbits=4;}
  
  tft.setTextSize(2);
  if (oldEbits!=newEbits     ) {
    tft.setCursor(307,  97);
    if ((newEbits &1)==0){ tft.setTextColor(ST77XX_RED );} else { tft.setTextColor(ST77XX_GREEN );}
    if (newEbits==4){tft.setTextColor(ST77XX_BLACK );}
    tft.println("W");
    tft.setCursor(307,  113);
    if ((newEbits &2)==0){ tft.setTextColor(ST77XX_RED );} else { tft.setTextColor(ST77XX_GREEN );}
    if (newEbits==4){tft.setTextColor(ST77XX_BLACK );}
    tft.println("E");
    oldEbits=newEbits;
  }

    //Serial.println("EBits   :");Serial.print(MB4Errors[0],DEC);
  tft.setTextColor(ST77XX_YELLOW) ;
  tft.setTextSize(3);

  
}



void loop()
{
  float XX,YY,PX,PY;
  // See if there's any  touch data for us
/* //SS Change//  remove old touch screen stuff  
  if (! ts.bufferEmpty()) {
      TS_Point p = ts.getPoint();
      posx = map(p.y, TS_MINX, TS_MAXX, 0, tft.width());
      posy = 240-map(p.x, TS_MINY, TS_MAXY, 0, tft.height());
      Press(posx,posy);   
      if (ShowDot){tft.fillCircle(posx, posy, PENRADIUS, ST77XX_MAROON);} 
  }
*/
  SPI.setDataMode(SPI_MODE3);
  if (Xpt.xpt2046_twice_read_xy(&TSXpos, &TSYpos)){
     //Serial.print("X   :");Serial.println(TSXpos,DEC);Serial.print("Y   :");Serial.println(TSYpos,DEC);
    /// Changed to floating pt.... but probably the sensor is just naff 
    /// posy=240-(TSXpos-TS_MINX)*24/((TS_MAXX-TS_MINX)/10);  //NB. X and Y reversed (i.e. in portrait)
    /// posx=(TSYpos-TS_MINY)*32/((TS_MAXY-TS_MINY)/10); 
     XX=TSXpos;YY=TSYpos;
     PY=240-(XX-TS_MINX)*240/((TS_MAXX-TS_MINX));  //NB. X and Y reversed (i.e. in portrait)
     PX=(YY-TS_MINY)*320/((TS_MAXY-TS_MINY));
     posy=PY;posx=PX;      
     //Serial.print("X>>>:");Serial.println(posx,DEC);Serial.print("Y>>>:");Serial.println(posy,DEC);    
     if (posy>0) {Press(posx,posy);}
     if (ShowDot){tft.fillCircle(posx, posy, PENRADIUS, ST77XX_GREEN);}
  }
  SPI.setDataMode(SPI_MODE0);

  if(su!=0){su=su+1;}//counter so that if button pressed down then we dont change ICHaus settings as this is slow
  if (su >4 ){ 
    MB4Bits[0]=absBits;MB4EBits[0]=Ebits;
    if (absMode==1){MB4Type[0]=2;}//set SSI
    else if (absMode==3){MB4Type[0]=3;}//set SSI
    else MB4Type[0]=1;//BISS
    SetupMB4Channel(0);
    Serial.println("ABS parameters changed");
    Serial.println("Bits   :");Serial.print(MB4Bits[0],DEC);
    Serial.println("EBits  :");Serial.print(MB4EBits[0],DEC);
    Serial.println("Mode   :");Serial.print(MB4Type[0],DEC);
    su=0;
  }

  delay(100);
  StartReadingMB4(0);
  delay(50);
  if (rawMode==1){AbsPos=MB4Num[0]-AbsZero;} else {AbsPos=MB4Num[0];}
  ReadMB4Data(0);
  
  if (incMode!=0){DrawInc();} //encoder0Pos=encoder0Pos+98536;
  if (absMode!=0){DrawAbs();} //AbsPos=AbsPos+78536;
  if (PrintFlag==1){
         PrintFlag=0;Serial.print(AbsPos,DEC);
         Serial.println("");
     }
  
  
}

//----------------------------------------------------------

// M B 4   S T U F F 
//Read on Byte from iC-MB4:
byte readRegister(byte thisRegister,byte Channel) {
  byte result = 0;   // result to return
  digitalWrite(ICHausPin, LOW);
  delay(1);
  result = SPI.transfer(RcommandPacket);  //read command
  result = SPI.transfer(thisRegister);    //which register
  result = SPI.transfer(0x00);            //one byte 
  digitalWrite(ICHausPin, HIGH);

  //if (Debug==1){Serial.print("Read: ");Serial.print(thisRegister,HEX);Serial.print(":");Serial.print(result,HEX);Serial.print(":");Serial.print(MB4ResetPin,HEX);Serial.print("\n");   }
  return(result);
}

//Write one Byte to iC-MB4
void writeRegister(byte thisRegister, byte thisValue,byte Channel) {
  byte result = 0;   // result to return
  digitalWrite(ICHausPin, LOW);
  delay(1);
  result = SPI.transfer(WcommandPacket);
  result = SPI.transfer(thisRegister);
  result = SPI.transfer(thisValue);
  digitalWrite(ICHausPin, HIGH);
  
  //if (Debug==1){Serial.print("Write ");Serial.print(thisRegister,HEX);Serial.print(":");Serial.print(thisValue,HEX);Serial.print(":");Serial.print(MB4Pins[Channel],HEX);Serial.print("\n");   }
}

//iC-MB4: Check Comms  //Comms 1=OK(Version<>0) 0=not tested 2=error
byte MB4CheckComms(byte Channel) {
  byte result = 0;
  result=readRegister(0xEB,Channel);
  MB4Version[Channel]=result;MB4Comm[Channel]=2;
  if (result!=0){MB4Comm[Channel]=1;}
  
    Serial.print("SPI Comms check to MB4:Chan");Serial.print(Channel,DEC);Serial.print(":");
    if (result==0){Serial.print("FAILED");}
    if (result!=0){Serial.print("OK");}
    Serial.print("\n");   
  
}



void SetupMB4Channel(byte Channel) {
  byte result = 0;     
  for (byte address = 0x00; address < 0xE0; address++){
    writeRegister(address, 0x00, Channel);
  }
  result = readRegister(0x00, Channel);
  // Configuration Slave n
    result= 64 + (MB4Bits[Channel] & 63) + (MB4EBits[Channel] & 15) - 1;
    if (MB4Type[Channel]==3){//If Gray then set the CRC
      result=result+128;
    } 
    writeRegister(0xC0, result, Channel);//7:enable scdx;6:grey;0:5-numbits//was 0x59// 26+2+6=34=>-1=33=//5F reads 1 byte to much for 24bit SSI
    result=0;
    if (MB4Type[Channel]==1){//If BissC then set the CRC
      result=0xA1;
    } 
    writeRegister(0xC1, result, Channel);//CRC stuff - was 0xA1
    writeRegister(0xC2, 0x00, Channel);  //CRC stuff
    writeRegister(0xC3, 0x00, Channel);  //CRC stuff
  // Control Communication Configuration
    writeRegister(0xE2, 0x78, Channel);
    writeRegister(0xE3, 0x07, Channel);
    writeRegister(0xE4, 0x01, Channel);
    writeRegister(0xE5, 0xC1, Channel);//7:reg comms;6:Biss C=1;3:5=slave num=1;//was 0xC1
  // Master Configuration
    result=(MB4Freq[Channel] & 0x1F)+0x80;
    writeRegister(0xE6, result, Channel);//works with 0x92(~300KHz)
    writeRegister(0xE7, 0x00, Channel);
    writeRegister(0xE8, 0x7C, Channel);
    writeRegister(0xE9, 0x00, Channel);
  // Channel Configuration
    writeRegister(0xEC, 0x01, Channel); 
    result= (MB4Type[Channel] & 3) * 5;
     if (MB4Type[Channel]==3){//If Gray then set the CRC
      result=10;
    }    
    writeRegister(0xED, result, Channel);//0x05 for BISS;0A for SSI
  // Slave Type Configuration
    writeRegister(0xEF, 0x00, Channel);
  // Instruction Register Reset
    writeRegister(0xF4, 0x00, Channel);
  // CFGIF 
    writeRegister(0xF5, 0x09, Channel);// Use internal 20MHz clock
  // BREAK Master
  //Serial.print("Break Master.");
    writeRegister(0xF4, 0x80, Channel);//The control register
    delay(1);  
  // INIT Master
  //Serial.print("Initialize Master.");
    writeRegister(0xF4, 0x10, Channel);//Init bit
    //Serial.print("Measured Line Delay:");
    result=(readRegister(0x00, Channel));
    // reset value of measured line delay
    writeRegister(0x00, 0x00, Channel);
    delay(1);  
    
    result=(readRegister(0xE8, Channel));//Choose one to check that it has been set with the correct value
    Serial.print("Channel Setup:");Serial.print(Channel,DEC);
    Serial.print(":");Serial.print(result,HEX);Serial.print("\n");
    if (result == 0x7C){MB4Comm[Channel]=1;} else {MB4Comm[Channel]=2;}  // use this as a check that was successful
    //MB4Comm[Channel]=2;
    //if (result==0x7C){MB4Comm[Channel]=1;}//set as OK if read back correct data - else will be left as 2
    //if (Debug==1){Serial.print("Channel setup ");Serial.print(Channel,DEC);Serial.print("Stat ");Serial.print(MB4Comm[Channel],DEC);Serial.print("\n");   }
    //if (Debug==1){Serial.print("pin ");Serial.print(MB4Pins[Channel],DEC);Serial.print("Stat ");Serial.print(result,DEC);Serial.print("\n");   }
}//end Setup MB4 Channel

void StartReadingMB4(byte Channel) {
  //AddrSel(Channel);
  //digitalWrite(MB4ResetPin[Channel], LOW);
  //digitalWrite(MB4ResetPin, HIGH);
  writeRegister(0xF4, 0x04, Channel);
  //digitalWrite(MB4ResetPin[Channel], HIGH);//release control
  //digitalWrite(MB4ResetPin, LOW);
}//StartReadingMB4

//      -----------------    R E A D     D A T A  ----------------------

void ReadMB4Data(byte Channel) {//Read the data (position)
  byte offset =0;
  long cnt =0;
  long rp =0;
  offset = (Channel << 3);
  MB4Num[Channel]=0;
  for (byte address = 0; address < 0x08; address++){
    MB4Raw[offset]=readRegister(address, Channel);
    if (address==0){ //first byte - contains error bits
      unsl=MB4Raw[offset];
      MB4Num[Channel]=unsl;
      if (MB4EBits[Channel]==0){MB4Errors[Channel]=3;}
      if (MB4EBits[Channel]==1){MB4Errors[Channel]=MB4Raw[offset] & 1;MB4Num[Channel]=unsl>>1;}
      if (MB4EBits[Channel]==2){MB4Errors[Channel]=MB4Raw[offset] & 3;MB4Num[Channel]=unsl>>2;}

    }
    else if (address<5){ //limit to 32 bits on arduino
      unsl=MB4Raw[offset];
      MB4Num[Channel]= MB4Num[Channel]+(unsl<<(address*8-MB4EBits[Channel]));

     //Serial.print (MB4Raw[offset],BIN);Serial.print ("<>>");
    }  
    //Serial.print (MB4Raw[offset],BIN);Serial.print ("<>>");Serial.print (MB4Num[Channel],DEC);Serial.print ("<>>");
    //Serial.println (" ");
    offset++;
  }
  //Serial.println (" ");
  //MB4Num[Channel]= MB4Num[Channel]>>MB4EBits[Channel]; //shift right to remove extra bits

  //if (Debug!=0) {Serial.print (readRegister(0xF0,Channel),BIN);Serial.print ("Status");}
  //if (   (readRegister(0xF0,Channel) & 1) == 0 ) {  //0xF0=0 then no response from encoder
  //    MB4Errors[Channel]=MB4Errors[Channel]+128;    //Add status error on bit 7;
  //}
  MB4StatF0[Channel]=readRegister(0xF0,Channel);
    
  //}

   
}//end ReadMB4Data








//-------------------------------------------------------

void doEncoder0() {
  if (digitalRead(encoder0PinA) == digitalRead(encoder0PinB)) {
    encoder0Pos++;
  } else {
    encoder0Pos--;
  }  
}

void doEncoder1() {
  if (digitalRead(encoder0PinA) == digitalRead(encoder0PinB)) {
    encoder0Pos--;
    //AbsPos++;
  } else {
    encoder0Pos++;
    //AbsPos--;
  }  
}


void doIndex2() {
  if (digitalRead(index0PinC) == 1) {
    IndexCount++;IndexState=1;    
  } else {
    IndexState=0;
  }  
}

void doPrint3() {
  PrintFlag=1;  
}
