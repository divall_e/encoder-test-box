EESchema Schematic File Version 4
LIBS:EncBox1-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:DB15_Female J1
U 1 1 6427D823
P 2350 3150
F 0 "J1" H 2257 2158 50  0000 C CNN
F 1 "DB15_F_Enc" H 2257 2249 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-15_Female_Horizontal_P2.77x2.54mm_EdgePinOffset9.40mm" H 2350 3150 50  0001 C CNN
F 3 " ~" H 2350 3150 50  0001 C CNN
	1    2350 3150
	-1   0    0    1   
$EndComp
$Comp
L Connector:DB9_Female J2
U 1 1 6427D888
P 2350 5050
F 0 "J2" H 2270 4358 50  0000 C CNN
F 1 "DB9_F_BISS" H 2270 4449 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-9_Female_Horizontal_P2.77x2.54mm_EdgePinOffset9.40mm" H 2350 5050 50  0001 C CNN
F 3 " ~" H 2350 5050 50  0001 C CNN
	1    2350 5050
	-1   0    0    1   
$EndComp
$Comp
L MCU_Module:Arduino_Leonardo A1
U 1 1 6427D937
P 7650 3100
F 0 "A1" H 7650 4278 50  0000 C CNN
F 1 "Arduino_Leonardo" H 7650 4187 50  0000 C CNN
F 2 "Module:Arduino_UNO_R3" H 7800 2050 50  0001 L CNN
F 3 "https://www.arduino.cc/en/Main/ArduinoBoardLeonardo" H 7450 4150 50  0001 C CNN
	1    7650 3100
	1    0    0    -1  
$EndComp
Text Label 2650 3650 0    50   ~ 0
CLK-
Text Label 2650 3450 0    50   ~ 0
DATA-
Text Label 2650 3250 0    50   ~ 0
I-
Text Label 2650 3050 0    50   ~ 0
B-
Text Label 2650 2850 0    50   ~ 0
A-
Text Label 2650 2650 0    50   ~ 0
5V
Text Label 2650 2450 0    50   ~ 0
5V
Text Label 2650 3750 0    50   ~ 0
GND
Text Label 2650 3550 0    50   ~ 0
CLK+
Text Label 2650 3350 0    50   ~ 0
DATA+
Text Label 2650 3150 0    50   ~ 0
I+
Text Label 2650 2950 0    50   ~ 0
B+
Text Label 2650 2750 0    50   ~ 0
A+
Text Label 2650 2550 0    50   ~ 0
SHIELD
$Comp
L Interface:TB5R1DW U1
U 1 1 64293A98
P 4650 3000
F 0 "U1" H 4650 4078 50  0000 C CNN
F 1 "26LS32" H 4650 3987 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 5650 2100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tb5r1.pdf" H 4650 3000 50  0001 C CNN
	1    4650 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J4
U 1 1 6429AD4D
P 6650 1150
F 0 "J4" H 6700 1467 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even_POSIC" H 6700 1376 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x04_P2.54mm_Vertical" H 6650 1150 50  0001 C CNN
F 3 "~" H 6650 1150 50  0001 C CNN
	1    6650 1150
	1    0    0    -1  
$EndComp
Text Label 6450 1050 2    50   ~ 0
5V
Text Label 6950 1050 0    50   ~ 0
GND
Text Label 6950 1150 0    50   ~ 0
ST_B
Text Label 6450 1250 2    50   ~ 0
ST_I
Text Label 6450 1150 2    50   ~ 0
ST_A
Text Label 5150 2400 0    50   ~ 0
ST_A
Text Label 5150 2700 0    50   ~ 0
ST_B
Text Label 5150 3000 0    50   ~ 0
ST_I
Text Label 2650 4650 0    50   ~ 0
5V
Text Label 2650 4850 0    50   ~ 0
5V
Text Label 2650 4750 0    50   ~ 0
GND
Text Label 2650 4950 0    50   ~ 0
GND
Text Label 2650 5050 0    50   ~ 0
CLK-
Text Label 2650 5250 0    50   ~ 0
CLK+
Text Label 2650 5350 0    50   ~ 0
DATA+
Text Label 2650 5150 0    50   ~ 0
DATA-
NoConn ~ 2650 5450
Text Label 7150 3700 2    50   ~ 0
MISO
Text Label 7150 3600 2    50   ~ 0
MOSI
Text Label 7150 3800 2    50   ~ 0
SCK
Text Label 7150 2700 2    50   ~ 0
ST_A
Text Label 7150 2800 2    50   ~ 0
ST_B
Text Label 7150 2900 2    50   ~ 0
TP_CS
Text Label 7150 3200 2    50   ~ 0
LCD_DC
Text Label 7150 3400 2    50   ~ 0
LCD_BL
Text Label 7150 3500 2    50   ~ 0
LCD_CS
Text Label 8150 3400 0    50   ~ 0
MB4_RST
Text Label 8150 3300 0    50   ~ 0
MB4_CS
Text Label 7850 2100 1    50   ~ 0
5V
Text Label 7550 4200 3    50   ~ 0
GND
Text Label 7650 4200 3    50   ~ 0
GND
Text Label 8100 5100 0    50   ~ 0
5V
Text Label 8100 5250 0    50   ~ 0
5V
Text Label 8100 5400 0    50   ~ 0
GND
Text Label 8100 5550 0    50   ~ 0
GND
Text Label 8100 5700 0    50   ~ 0
DATA+
Text Label 8100 5850 0    50   ~ 0
DATA-
Text Label 8100 6000 0    50   ~ 0
CLK-
Text Label 8100 6150 0    50   ~ 0
CLK+
Text Label 7150 5100 2    50   ~ 0
MB4_CS
Text Label 7150 5250 2    50   ~ 0
SCK
Text Label 7150 5400 2    50   ~ 0
MOSI
Text Label 7150 5550 2    50   ~ 0
MISO
Text Label 7150 5850 2    50   ~ 0
MB4_RST
NoConn ~ 7150 5700
NoConn ~ 7150 6000
NoConn ~ 7150 6150
NoConn ~ 8150 2500
NoConn ~ 8150 2700
NoConn ~ 8150 2900
NoConn ~ 8150 3200
NoConn ~ 8150 3800
NoConn ~ 8150 3900
Text Label 8150 1300 2    50   ~ 0
GND
Text Label 8150 1200 2    50   ~ 0
12V
Text Label 8150 1400 2    50   ~ 0
5V
$Comp
L Connector:Screw_Terminal_01x08 J3
U 1 1 6431EDD8
P 3300 1550
F 0 "J3" H 3220 925 50  0000 C CNN
F 1 "Screw_Terminal_01x08" H 3220 1016 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-8-5.08_1x08_P5.08mm_Horizontal" H 3300 1550 50  0001 C CNN
F 3 "~" H 3300 1550 50  0001 C CNN
	1    3300 1550
	-1   0    0    1   
$EndComp
Text Label 3500 1850 0    50   ~ 0
GND
Text Label 3500 1750 0    50   ~ 0
5V
Text Label 3500 1650 0    50   ~ 0
A-
Text Label 3500 1550 0    50   ~ 0
A+
Text Label 3500 1450 0    50   ~ 0
B-
Text Label 3500 1350 0    50   ~ 0
B+
Text Label 3500 1250 0    50   ~ 0
I-
Text Label 3500 1150 0    50   ~ 0
I+
Text Label 4150 2400 2    50   ~ 0
A+
Text Label 4150 2500 2    50   ~ 0
A-
Text Label 4150 2700 2    50   ~ 0
B+
Text Label 4150 2800 2    50   ~ 0
B-
Text Label 4150 3000 2    50   ~ 0
I+
Text Label 4150 3100 2    50   ~ 0
I-
$Comp
L power:+12V #PWR0101
U 1 1 6431F9B5
P 9250 1200
F 0 "#PWR0101" H 9250 1050 50  0001 C CNN
F 1 "+12V" H 9265 1373 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D3.50mm_Drill1.4mm_Beaded" H 9250 1200 50  0001 C CNN
F 3 "" H 9250 1200 50  0001 C CNN
	1    9250 1200
	1    0    0    -1  
$EndComp
Text Label 9250 1200 3    50   ~ 0
12V
$Comp
L power:+5V #PWR0102
U 1 1 6431FA1C
P 9550 1200
F 0 "#PWR0102" H 9550 1050 50  0001 C CNN
F 1 "+5V" H 9565 1373 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D3.50mm_Drill1.4mm_Beaded" H 9550 1200 50  0001 C CNN
F 3 "" H 9550 1200 50  0001 C CNN
	1    9550 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 6431FA6A
P 9800 1200
F 0 "#PWR0103" H 9800 950 50  0001 C CNN
F 1 "GND" H 9805 1027 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D3.50mm_Drill1.4mm_Beaded" H 9800 1200 50  0001 C CNN
F 3 "" H 9800 1200 50  0001 C CNN
	1    9800 1200
	1    0    0    -1  
$EndComp
Text Label 9550 1200 3    50   ~ 0
5V
Text Label 9800 1200 1    50   ~ 0
GND
$Comp
L Device:C C1
U 1 1 6431FCD6
P 10150 1150
F 0 "C1" H 10265 1196 50  0000 L CNN
F 1 "C" H 10265 1105 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L10.0mm_D4.5mm_P15.00mm_Horizontal" H 10188 1000 50  0001 C CNN
F 3 "~" H 10150 1150 50  0001 C CNN
	1    10150 1150
	1    0    0    -1  
$EndComp
Text Label 10150 1000 1    50   ~ 0
5V
Text Label 10150 1300 3    50   ~ 0
GND
$Comp
L power:GNDS #PWR0104
U 1 1 6432057C
P 10550 1200
F 0 "#PWR0104" H 10550 950 50  0001 C CNN
F 1 "GNDS" H 10555 1027 50  0000 C CNN
F 2 "" H 10550 1200 50  0001 C CNN
F 3 "" H 10550 1200 50  0001 C CNN
	1    10550 1200
	1    0    0    -1  
$EndComp
Text Label 10550 1200 1    50   ~ 0
SHIELD
NoConn ~ 4150 3300
NoConn ~ 4150 3400
NoConn ~ 5150 3300
NoConn ~ 7150 3300
NoConn ~ 7150 3100
Text Label 4150 3600 2    50   ~ 0
EN
Text Label 4150 3700 2    50   ~ 0
NEN
Wire Notes Line
	7850 600  7850 1750
Wire Notes Line
	7850 1750 10900 1750
Wire Notes Line
	10900 1750 10900 600 
Wire Notes Line
	10900 600  7850 600 
Text Notes 8150 700  2    50   ~ 0
POWER
$Comp
L Device:R R4
U 1 1 64320F64
P 5350 6700
F 0 "R4" V 5557 6700 50  0000 C CNN
F 1 "10K" V 5466 6700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5280 6700 50  0001 C CNN
F 3 "~" H 5350 6700 50  0001 C CNN
	1    5350 6700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 643211CE
P 4500 6700
F 0 "R3" V 4707 6700 50  0000 C CNN
F 1 "20K" V 4600 6700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4430 6700 50  0001 C CNN
F 3 "~" H 4500 6700 50  0001 C CNN
	1    4500 6700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 64321258
P 5350 6250
F 0 "R2" V 5557 6250 50  0000 C CNN
F 1 "10K" V 5466 6250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5280 6250 50  0001 C CNN
F 3 "~" H 5350 6250 50  0001 C CNN
	1    5350 6250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 6432129C
P 4500 6250
F 0 "R1" V 4707 6250 50  0000 C CNN
F 1 "20K" V 4616 6250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4430 6250 50  0001 C CNN
F 3 "~" H 4500 6250 50  0001 C CNN
	1    4500 6250
	0    -1   -1   0   
$EndComp
Text Label 4350 6250 2    50   ~ 0
5V
Text Label 4350 6700 2    50   ~ 0
5V
Text Label 5500 6250 0    50   ~ 0
GND
Text Label 5500 6700 0    50   ~ 0
GND
Text Label 4650 6250 0    50   ~ 0
A-
Text Label 4650 6700 0    50   ~ 0
B-
Text Label 5200 6250 2    50   ~ 0
A-
Text Label 5200 6700 2    50   ~ 0
B-
$Comp
L Device:R R6
U 1 1 64321AF9
P 5350 7150
F 0 "R6" V 5557 7150 50  0000 C CNN
F 1 "10K" V 5466 7150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5280 7150 50  0001 C CNN
F 3 "~" H 5350 7150 50  0001 C CNN
	1    5350 7150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 64321B00
P 4500 7150
F 0 "R5" V 4707 7150 50  0000 C CNN
F 1 "20K" V 4600 7150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4430 7150 50  0001 C CNN
F 3 "~" H 4500 7150 50  0001 C CNN
	1    4500 7150
	0    -1   -1   0   
$EndComp
Text Label 4350 7150 2    50   ~ 0
5V
Text Label 5500 7150 0    50   ~ 0
GND
Text Label 4650 7150 0    50   ~ 0
I-
Text Label 5200 7150 2    50   ~ 0
I-
NoConn ~ 6950 1250
NoConn ~ 6950 1350
NoConn ~ 6450 1350
$Comp
L EdwinsLibrary_0:DC-DC U3
U 1 1 6431A390
P 8150 1550
F 0 "U3" H 8627 1846 50  0000 L CNN
F 1 "DC-DC" H 8627 1755 50  0000 L CNN
F 2 "EncFootPrints:DC-DC" H 8150 1550 50  0001 C CNN
F 3 "" H 8150 1550 50  0001 C CNN
	1    8150 1550
	1    0    0    -1  
$EndComp
$Comp
L EdwinsLibrary_0:ICHAUS_Module U2
U 1 1 6431AD31
P 7300 5100
F 0 "U2" H 7625 5465 50  0000 C CNN
F 1 "ICHAUS_Module" H 7625 5374 50  0000 C CNN
F 2 "EncFootPrints:ICHAUS_module" H 7800 5250 50  0001 C CNN
F 3 "" H 7800 5250 50  0001 C CNN
	1    7300 5100
	1    0    0    -1  
$EndComp
Text Label 4650 3900 0    50   ~ 0
GND
NoConn ~ 7550 2100
NoConn ~ 7750 2100
Text Label 7150 3000 2    50   ~ 0
SD_CS
Text Label 7150 2600 2    50   ~ 0
D1
Text Label 7150 2500 2    50   ~ 0
D0
NoConn ~ 7150 2500
NoConn ~ 7150 2600
NoConn ~ 7150 3000
NoConn ~ 7150 3400
NoConn ~ 7150 3500
NoConn ~ 7150 2900
NoConn ~ 7150 3200
Wire Wire Line
	7750 4200 7650 4200
Text Label 2650 3850 0    50   ~ 0
12V
Text Label 8150 3500 0    50   ~ 0
EN
Text Label 8150 3600 0    50   ~ 0
NEN
Text Label 4650 2100 0    50   ~ 0
5V
$Comp
L Connector:TestPoint TP3
U 1 1 6432884C
P 5650 4650
F 0 "TP3" H 5708 4770 50  0000 L CNN
F 1 "GND" H 5708 4679 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D3.80mm_Drill2.0mm" H 5850 4650 50  0001 C CNN
F 3 "~" H 5850 4650 50  0001 C CNN
	1    5650 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 64328853
P 6250 4650
F 0 "TP4" H 6308 4770 50  0000 L CNN
F 1 "12V" H 6308 4679 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D3.80mm_Drill2.0mm" H 6450 4650 50  0001 C CNN
F 3 "~" H 6450 4650 50  0001 C CNN
	1    6250 4650
	1    0    0    -1  
$EndComp
Text Label 5650 4650 2    50   ~ 0
GND
Text Label 6250 4650 2    50   ~ 0
12V
Text Label 8150 3100 0    50   ~ 0
ST_I
$Comp
L Jumper:SolderJumper_2_Open JP1
U 1 1 643297F0
P 9350 3350
F 0 "JP1" H 9350 3555 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 9350 3464 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 9350 3350 50  0001 C CNN
F 3 "~" H 9350 3350 50  0001 C CNN
	1    9350 3350
	1    0    0    -1  
$EndComp
Text Label 9500 3350 0    50   ~ 0
D0
Text Label 9200 3350 2    50   ~ 0
ST_I
$EndSCHEMATC
