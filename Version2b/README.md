# Materials

## _Arduino UNO_
(Plan was to use Leonardo, but hadn't realised it only uses the ICP plug for SPI communications, so it was left off the first test run of 5 PCBs)

## _2.8inch TFT Touch Shield_
(Previous LCD touch sheild was not available so swapped to this one)
https://www.waveshare.com/wiki/2.8inch_TFT_Touch_Shield
Features
Driver chip	ST7789V
Interface	SPI
Resolution	320 *240
Display size	57.6mm*43.2mm



Had to change to use different graphics library...
```sh
//SS Change// #include <Adafruit_ILI9341.h>
//SS Change// #include <Adafruit_STMPE610.h>
#include <Adafruit_ST7789.h> // Hardware-specific library for ST7789
#include "XPT2046.h"         // touch stuff
```
Also have to ensure that brightness is turned up or it will just stay black- previous screen did not need this.

## _Smaller components_

- DC-DC convertor to produce 5V for Arduino and encoders. (Distrelec 301-85-971)
- Arduino Stcking headers (Distrelec 301-63-264)
- Differential to single ended convertor for incremental encoder inputs. 4 RS422 Empfänger (26LS32 from stores )
- Resistors 10k & 20k to give bias level so will work with single or differential incremental encoder signals.
- 3.5x16 Blechschrauben from stores to hold down PCB
- Connector block (i.e. Compona 262 072-2, but 8 screw ports)
- DB9 pcb connector (Female for the BISS) (Distrelec 144-16-924 )
- DB15 pcb connector (Female encoder std) (Distrelec 144-16-921 )
- Two Sockets for BISS Module (Distrelec 143-95-730)
- Socket for IC (IC Sockel, 16-polig, Lötanschluss from Stores)
- Power input connector. Digi-Key Part Number: 486-3380-ND
- ~150uF capacitor


# Future

DB9 and DB15 connectors to be changed to be from same series. (Currently one is a lower profile.)
Use Leonardo board as have some spares - schematic need changing, see above.

Add same connector as Beckhoff encoders for better compatibility in SLS.
Add ability to switch to a 9V supply as apparently Beckhoff modules can do this. (DC-DCs purchased.)

Posic connector is wired incorrectly... pcb schematic needs reworking.

12V battery pack... 

