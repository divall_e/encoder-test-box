# Encoder Test Boxes   ( + Power Pack)

Small test boxes into which PSI standard encoders (incremental or absolute) can be plugged to see if they are working.
They have a PSI standard encoder socket into which either type of encoder can be plugged. An external 12V PSU is requires to run them.
All version support both BISS and SSI.

### _Version 1_
Box with small 1.3" screen and 2 buttons to select/change options.
Standard encoder socket + 12V input.
Uses Protura (now Joule Fusion) SSI->RS485 board to decode the absolute encoders.
Incremental encoders changed from differential to single ended and wired straight to microcontroller.

### _Version 2a_
Screen updated to 2.7" touch panel.
Standard encoder socket + 12V input.
Uses PSI standard ICHaus module to read absolute encoders.

### _Version 2b_
Screen swapped to nominally "identical" one from Waveshare - currently touch is not so good...
Added a BISS encoder socket so Renishaw encoders can be plugged in directly.
Now using custom PCB. Also socket for Posic encoders and screw terminals for incremental encoders inside the box.
Uses PSI standard ICHaus module to read absolute encoders.




# Version 1
![alt image](https://gitlab.psi.ch/divall_e/encoder-test-box/-/raw/master/ver1sm.JPG)


# Version 2a
![alt image](https://gitlab.psi.ch/divall_e/encoder-test-box/-/raw/master/ver2a.jpg)


# Version 2b
![alt image](https://gitlab.psi.ch/divall_e/encoder-test-box/-/raw/master/ver2bsm.JPG)

# Power Pack
![alt image](https://gitlab.psi.ch/divall_e/encoder-test-box/-/raw/master/PSUs.JPG)
