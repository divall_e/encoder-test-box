12 V transportable power supply for all encoder readers

> Lead acid power battery (12V 1.2 Amp Hours)


Components:-

Battery>
  Fiamm Pb-12-1,2-4,8 FG20121 VRLA 12 V 1.2 Ah AGM (W x H x D) 97 x 57 x 48 mm 4.8 mm blade terminal Maintenance-free, Low (Conrad 23ChF)
  https://www.conrad.ch/de/p/fiamm-pb-12-1-2-4-8-fg20121-bleiakku-12-v-1-2-ah-blei-vlies-agm-b-x-h-x-t-97-x-57-x-48-mm-flachstecker-4-8-mm-wartun-1694699.html 

Charger>
  Dino KRAFTPAKET 136311 Automatic charger 12 V, 6 V 0.8 A 3.8 A  (Conrad 30ChF)
  https://www.conrad.ch/de/p/dino-kraftpaket-136311-automatikladegeraet-12-v-6-v-0-8-a-3-8-a-1784096.html

Cable>
  Power Plug with Cable 500mA, 12V, 5.5mm
  https://www.distrelec.ch/en/power-plug-with-cable-500ma-12v-5mm-cable-length-8m-bare-end-marushin-electric-mc-2374l/p/14205175

Connectors>
   2-pin lemo  
